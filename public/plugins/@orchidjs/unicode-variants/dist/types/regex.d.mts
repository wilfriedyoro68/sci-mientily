export function arrayToPattern(chars: string[]): string;
export function sequencePattern(array: string[]): string;
export function setToPattern(chars: Set<string>): string;
export function hasDuplicates(array: any[]): boolean;
export function escape_regex(str: string): string;
export function maxValueLength(array: string[]): number;
export function unicodeLength(str: string): number;
export function toArray(p: any): any[];
