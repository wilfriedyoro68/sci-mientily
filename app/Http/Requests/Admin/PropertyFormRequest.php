<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PropertyFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [

            'title' => ['required', 'string', 'min:8'],
            'description' => ['required', 'string', 'min:25'],
            'surface' => ['required', 'numeric', 'min:4'],
            'rooms' => ['required', 'numeric', 'min:1'],
            'bedrooms' => ['required', 'numeric', 'min:1'],
            'floor' => ['required', 'numeric', 'min:0'],
            'price_ht' => ['numeric'],
            'price_ttc' => ['required', 'numeric'],
            'address' => ['required', 'string', 'min:10'],
            'quarter_id' => ['required', 'numeric'],
            'options' => ['array', 'exists:options,id'],

        ];
    }
}
