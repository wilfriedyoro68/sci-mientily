<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Property extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'slug',
        'quarter_id',
        'description',
        'surface',
        'rooms',
        'bedrooms',
        'floor',
        'price_ht',
        'price_ttc',
        'address',
        'is_rent',
        'is_sold',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'title' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'surface' => 'integer',
        'rooms' => 'integer',
        'bedrooms' => 'integer',
        'floor' => 'integer',
        'price_ht' => 'integer',
        'price_ttc' => 'integer',
        'address' => 'integer',
        'is_rent' => 'boolean',
        'is_sold' => 'boolean',
    ];

    /**
     * Get the quarter that owns the property
     */
    public function quarter(): BelongsTo
    {
        return $this->belongsTo(Quarter::class);
    }

    /**
     * Get these options
     *
     * @return BelongsToMany<Option>
     */
    public function options() : BelongsToMany
    {
        return $this->belongsToMany(Option::class);
    }
}
