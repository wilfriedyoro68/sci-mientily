<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('quarter_id')->constrained();
            $table->string('title', 100);
            $table->string('slug')->nullable();
            $table->longText('description');
            $table->integer('surface');
            $table->smallInteger('rooms');
            $table->smallInteger('bedrooms');
            $table->smallInteger('floor');
            $table->integer('price_ht')->nullable();
            $table->integer('price_ttc');
            $table->string('address');
            $table->boolean('is_rent')->default(false);
            $table->boolean('is_sold')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
};
