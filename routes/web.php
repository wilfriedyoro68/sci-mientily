<?php

use App\Http\Controllers\Admin\OptionController;
use App\Http\Controllers\Admin\PropertyController;
use App\Http\Controllers\Blog\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PostController::class, 'index']);

Route::get('/test', function () {
    return view('welcome');
});

// Route::prefix('/articles')->name('blog.')->group(function () {

//     Route::get('/blog/{id}-{slug}', function (int $id, string $slug) {

//         return [
//             'id' => $id,
//             'slug' => $slug,
//         ];

//     })->where([
//         'id' => '[0-9]+',
//         'slug' => '[a-z0-9\-]+',
//     ])->name('show');

// });

Route::prefix('admin')->name('admin.')->group(function () {

    Route::resource('properties', PropertyController::class);

    Route::resource('options', OptionController::class);

});
