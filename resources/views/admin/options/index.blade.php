@extends('layouts.admin')

@section('title', 'Tous les commodités')

@section('content')

    <div class="d-flex justify-content-between align-items-center">

        <h1>Les commodités</h1>

        <a href="#" class="btn btn-primary">Ajouter une commodité</a>

    </div>

    <table class="table table-striped">

        <thead>
            <tr>
                <td>Titre</td>
                <td>Description</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
            @forelse ($options as $option)
                <tr>
                    <td>{{ $option->name }}</td>
                    <td>{{ $option->description }}</td>
                    <td>

                        <div class="d-flex gap-2 w-100 justify-content-end">

                            <a href="{{ route('admin.options.edit', $option) }}" class="btn btn-warning">
                                <i class="fa-solid fa-pen"></i>
                            </a>
                            <form action="{{ route('admin.options.destroy', $option) }}">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">
                                    <i class="fa-solid fa-trash"></i>
                                </button>
                            </form>

                        </div>

                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="6"> Aucun bien disponible</td>
                </tr>
            @endforelse
        </tbody>

    </table>

    {{-- {{ $options->links() }} --}}

@endsection
