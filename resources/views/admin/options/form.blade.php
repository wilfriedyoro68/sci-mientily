@extends('layouts.admin')

@section('title', $option->exists ? 'Editer la commodité' : 'Ajouter une commodité')

@section('content')

    <div class="d-flex justify-content-between align-items-center">

        <h1>@yield('title')</h1>

        <a href="{{ route('admin.options.index') }}" class="btn btn-default">Retour</a>

    </div>

    <form action="{{ route($option->exists ? 'admin.options.update' : 'admin.options.store', ['option' => $option]) }}">

        @csrf
        @method($option->exists ? 'PATCH' : 'POST')

        <div class="row">

            @include('shared.input', ['class' => 'col', 'label' => 'Titre', 'name' => 'name', 'value' => $option->name])

            @include('shared.input', ['class' => 'col', 'type' => 'textarea', 'name' => 'description', 'value' => $option->description])


        </div>

        <div>
            @if ($option->exists)
                <input type="submit" class="btn btn-warning" value="Modifier">
            @else
                <input type="submit" class="btn btn-success" value="Valider">
            @endif
        </div>


    </form>



@endsection
