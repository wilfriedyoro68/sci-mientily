@extends('layouts.admin')

@section('title', 'Tous le biens')

@section('content')

    <div class="d-flex justify-content-between align-items-center">

        <h1>Les biens</h1>

        <a href="#" class="btn btn-primary">Ajouter un bien</a>

    </div>

    <table class="table table-striped">

        <thead>
            <tr>
                <td>Titre</td>
                <td>Localisation</td>
                <td>Surface (m<sup>2</sup>)</td>
                <td>Prix de vente (F. CFA)</td>
                <td>Actions</td>
            </tr>
        </thead>
        <tbody>
            @forelse ($properties as $property)
                <tr>
                    <td>{{ $property->title }}</td>
                    <td>{{ $property->address .' '.$property->quarter->name }}</td>
                    <td>{{ $property->surface }}</td>
                    <td>{{ number_format($property->price_ttc, thousands_separator: '.') }}</td>
                    <td>

                        <div class="d-flex gap-2 w-100 justify-content-end">

                            <a href="{{ route('admin.properties.edit', $property) }}" class="btn btn-warning">
                                <i class="fa-solid fa-pen"></i>
                            </a>
                            <form action="{{ route('admin.properties.destroy', $property) }}">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">
                                    <i class="fa-solid fa-trash"></i>
                                </button>
                            </form>

                        </div>

                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="6"> Aucun bien disponible</td>
                </tr>
            @endforelse
        </tbody>

    </table>

    {{-- {{ $properties->links() }} --}}

@endsection
