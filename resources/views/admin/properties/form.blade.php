@extends('layouts.admin')

@section('title', $property->exists ? 'Editer un bien' : 'Ajouter un bien')

@section('content')

    <div class="d-flex justify-content-between align-items-center">

        <h1>@yield('title')</h1>

        <a href="{{ route('admin.properties.index') }}" class="btn btn-default">Retour</a>

    </div>

    <form action="{{ route($property->exists ? 'admin.properties.update' : 'admin.properties.store', ['property' => $property]) }}">

        @csrf
        @method($property->exists ? 'PATCH' : 'POST')

        <div class="row">

            @include('shared.input', ['class' => 'col', 'label' => 'Titre', 'name' => 'title', 'value' => $property->title])

            <!-- Surface and price -->
            <div class="col row">

                @include('shared.input', ['class' => 'col', 'name' => 'surface', 'value' => $property->surface])

                @include('shared.input', ['class' => 'col', 'label' => 'Prix', 'name' => 'price_ttc', 'value' => $property->price_ttc])

            </div>

            @include('shared.input', ['class' => 'col', 'type' => 'textarea', 'name' => 'description', 'value' => $property->description])

            <!-- Rooms, bedrooms and floor -->
            <div class="row">

                @include('shared.input', ['class' => 'col', 'label' => 'Pièces', 'name' => 'rooms', 'value' => $property->rooms])

                @include('shared.input', ['class' => 'col', 'label' => 'Chambres', 'name' => 'bedrooms', 'value' => $property->bedrooms])

                @include('shared.input', ['class' => 'col', 'label' => 'Etages', 'name' => 'floor', 'value' => $property->floor ])

            </div>

            <!-- Location (quarter and address) -->
            <div class="row">

                @include('shared.input', ['class' => 'col', 'label' => 'Quartier', 'name' => 'quarter_id', 'value' => $property->quarter_id])

                @include('shared.input', ['class' => 'col', 'label' => 'Addresse', 'name' => 'address', 'value' => $property->address])

            </div>

            <!-- Status (rent or sold) and Options -->
            <div class="row">

                @include('shared.select', ['class' => 'col', 'label' => 'Commodités', 'name' => 'options', 'value' => $property->options()->pluck('id'), 'multiple' => true, 'options' => $options])

                @include('shared.checkbox', ['class' => 'col', 'label' => 'Loué', 'name' => 'is_rent', 'value' => $property->is_rent])

                @include('shared.checkbox', ['class' => 'col', 'label' => 'Vendu', 'name' => 'is_sold', 'value' => $property->is_sold])

            </div>

        </div>

        <div>
            @if ($property->exists)
                <input type="submit" class="btn btn-warning" value="Modifier">
            @else
                <input type="submit" class="btn btn-success" value="Valider">
            @endif
        </div>


    </form>



@endsection
