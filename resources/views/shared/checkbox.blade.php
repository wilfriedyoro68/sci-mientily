@php

    $class ??= null;
    $name ??= '';
    $label ??= '';
    $value ??= '';

@endphp

<div @class(['form-check form-switch', $class])>

    <label class="form-check-label" for="{{ $name }}">{{ $label }}</label>
    <input type="hidden" value="0" name="{{ $name }}">
    <input @checked(old($name, $value ?? false)) id="{{ $name }}" type="checkbox" value="1" name="{{ $name }}" class="form-check-input @error($name) is-invalid @enderror" role="switch">

    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
