@php

    $class ??= null;
    $name ??= '';
    $value ??= '';
    $label ??= ucfirst($name);
    $multiple ??= 'false';

@endphp



<div class="form-group">
    <label for="{{ $name }}">{{ $label }}</label>

    @if ($multiple == true)

        <select @class(['form-control', $class , @error($name) 'is-invalid' @enderror]) name="{{ $name }}[]" id="{{ $name }}" multiple>

            @forelse ($options as $key => $v)

                <option @selected($value->contains($key)) value="{{ $key }}"> {{ $v }}</option>

            @empty
                <option value=""> Aucune donnée disponible</option>
            @endforelse

        </select>

    @else

        <select @class(['form-control', $class , @error($name) 'is-invalid' @enderror]) name="{{ $name }}" id="{{ $name }}">

            @forelse ($options as $option)

                <option @selected($value->contains($option->id)) value="{{ $option->id }}"> {{ $option->name }}</option>

            @empty
                <option> Aucune donnée disponible</option>
            @endforelse

        </select>

    @endif

    @error($name)
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror
</div>
